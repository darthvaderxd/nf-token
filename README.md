# nf-token

NilFactor Token is a simple package to allow you to add an api token to your restify or express server.

```
//...
var token = require('nf-token');
server.pre(token.verifyRequest);
//...
```

Create a config directory in your project with a json file called development.json or <NODE_ENV>.json whatever your NODE_ENV=thisname with the following

```
{
    "nfToken": {
        "apiKey": "key",
        "apiToken": "token1"
    }
}
```

Then when you make requests to your api create a header key/pair value of "key: token1" (req.headers.key = "token1"), if using the above example.

To allow routes that do not require a token simply doe

```
token.addIgnoreRoute('/');
token.addIgnoreROute('/hello/*');
```
