const config = require('config-node')();
const HTTPStatus = require('http-status');

const ignored_routes = [];

/**
 * Add a route to allow without a token
 * @param string route - the route ie /hello/:world/
 */
const addIgnoreRoute = (route) => {
    ignored_routes.push(route);
};

const getIgnoredRoutes = () => ignored_routes;


const matchRuleShort = (str, rule) => {
    if (str && rule) {
        const ruleComp = rule.split('*').join('.*');
        return new RegExp(`^${ruleComp}$`).test(str);
    }

    return false;
};

/**
 * Should this request be allowed with or without a token
 * @param string uri - the request uri/url
 * @return boolean
 */
const checkIgnored = (uri) => {
    if (ignored_routes.length > 0) {
        for (let i = 0; i < ignored_routes.length; i++) { // eslint-disable-line no-plusplus
            const result = matchRuleShort(uri, ignored_routes[i]);
            if (result) {
                return true;
            }
        }
    }

    return false;
};

/**
 * Get the remote ip address of the client
 * @param object request - the request object
 * @return string
 */
const getRemoteIP = (request) => {
    if (typeof request !== 'undefined' && typeof request.connection !== 'undefined') {
        const ip = request.headers['x-forwarded-for'] || request.connection.remoteAddress;
        return ip.replace('::ffff:', '');
    }

    return 'ip address n/a';
};

const handleResponse = (req, res, next) => {
    const ip = getRemoteIP(req);
    const response = {
        code: 'UnauthorizedRequest',
        result: [{ error: 'Incorrect api key' }],
    };

    res.statusCode = HTTPStatus.UNAUTHORIZED;
    res.send(HTTPStatus.UNAUTHORIZED, response);

    if (!process.env.SILENT) {
        console.log('NilFactor Token => request to %s denied for %s', req.url, ip);
    }

    if (typeof next === 'function') {
        return next();
    }

    return false;
};

/**
 * Verify the request has proper authentication
 * @param object req the request object
 * @param object res the response object
 * @param Function next the next function to happen
 */
const verifyRequest = (req, res, next) => {
    const key = config.nfToken.apiKey;
    const token = config.nfToken.apiToken;

    if (checkIgnored(req.url)) {
        return next();
    }

    if (typeof req.headers[key] !== 'undefined' && req.headers[key] === token) {
        return next();
    }

    return handleResponse(req, res, next);
};

module.exports.addIgnoreRoute = addIgnoreRoute;
module.exports.getIgnoredRoutes = getIgnoredRoutes;
module.exports.verifyRequest = verifyRequest;
